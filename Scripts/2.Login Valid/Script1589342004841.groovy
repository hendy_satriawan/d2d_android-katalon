import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.callTestCase(findTestCase('onboarding'), [:], FailureHandling.STOP_ON_FAILURE)

'langsung cek untuk jalan di staging atau production'
if (GlobalVariable.Environtment_production == true) {
    println('data production')

    Mobile.waitForElementPresent(findTestObject('Login/Login - input email'), GlobalVariable.g_timeout)

    Mobile.setText(findTestObject('Login/Login - input email'), GlobalVariable.g_email_login_prod, 0)

    Mobile.waitForElementPresent(findTestObject('Login/Login - input password'), GlobalVariable.g_timeout)

    Mobile.setText(findTestObject('Login/Login - input password'), GlobalVariable.g_pass_login_prod, 0)
} else {
    println('data staging')

    Mobile.waitForElementPresent(findTestObject('Login/Login - input email'), GlobalVariable.g_timeout)

    Mobile.setText(findTestObject('Login/Login - input email'), GlobalVariable.g_email_login_staging, 0)

    Mobile.waitForElementPresent(findTestObject('Login/Login - input password'), GlobalVariable.g_timeout)

    Mobile.setText(findTestObject('Login/Login - input password'), GlobalVariable.g_pass_login_staging, 0)
}

Mobile.waitForElementPresent(findTestObject('Login/Login - button login'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Login/Login - button login'), 0)

Mobile.verifyElementExist(findTestObject('Feeds/Feeds - text feeds'), GlobalVariable.g_timeout)

Mobile.closeApplication()

