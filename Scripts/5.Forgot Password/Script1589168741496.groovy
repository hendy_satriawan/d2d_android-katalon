import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.callTestCase(findTestCase('onboarding'), [:], FailureHandling.STOP_ON_FAILURE)

def deviceHight = Mobile.getDeviceHeight()

def deviceWidth = Mobile.getDeviceWidth()

int startX = deviceWidth / 2

int endX = startX

int startY = deviceWidth

int endY = deviceWidth * 0.35

Mobile.waitForElementPresent(findTestObject('Login/Login - link forgot password'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Login/Login - link forgot password'), 0)

Mobile.waitForElementPresent(findTestObject('Forgot Password/Forgot - text forgot password'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Forgot Password/Forgot - input Email'), GlobalVariable.g_timeout)

Mobile.setText(findTestObject('Forgot Password/Forgot - input Email'), v_email, 0)

Mobile.tap(findTestObject('Forgot Password/Forgot - button Submit'), 0)

Mobile.comment('buka chrome untuk akses email & input password baru')

Mobile.startExistingApplication('com.android.chrome', FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Chrome/Chrome - Button expand tab'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Chrome - Button expand tab'), 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Chrome - Add new tab'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Chrome - Add new tab'), 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Chrome - Input Search Google'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Chrome - Input Search Google'), 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Chrome - Input Url Bar'), GlobalVariable.g_timeout)

Mobile.setText(findTestObject('Chrome/Chrome - Input Url Bar'), 'yopmail.com'+'\\n', 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - icon header yopmail'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - input email yopmail'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Yopmail - input email yopmail'), 0)

Mobile.setText(findTestObject('Chrome/Yopmail - input email yopmail'), v_email+'\\n', 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - icon trash (hapus email)'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - Email yang diterima'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Yopmail - Email yang diterima'), 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - text Title reset password'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Chrome/yopmail - text code forgot password'), GlobalVariable.g_timeout)

def code_forgot = Mobile.getText(findTestObject('Chrome/yopmail - text code forgot password'), 0)

println(code_forgot)

Mobile.pressBack()

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - select all email'), 0)

Mobile.tap(findTestObject('Chrome/Yopmail - select all email'), 0)

Mobile.tap(findTestObject('Chrome/Yopmail - icon trash (hapus email)'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

while (Mobile.waitForElementPresent(findTestObject('Forgot Password/Forgot - input code'), 2, FailureHandling.STOP_ON_FAILURE) == 
false) {
    Mobile.pressBack()
}

println(code_forgot)

Mobile.waitForElementPresent(findTestObject('Forgot Password/Forgot - text check your email'), GlobalVariable.g_timeout)

Mobile.setText(findTestObject('Forgot Password/Forgot - input code'), code_forgot, 0)

Mobile.setText(findTestObject('Forgot Password/Forgot - input new password'), GlobalVariable.g_email_pass, 0)

Mobile.setText(findTestObject('Forgot Password/Forgot - input password confirmation'), GlobalVariable.g_email_pass, 0)

while (Mobile.waitForElementPresent(findTestObject('Forgot Password/Forgot - button submit code'), 2, FailureHandling.CONTINUE_ON_FAILURE) == 
false) {
    Mobile.swipe(startX, startY, endX, endY)
}

Mobile.waitForElementPresent(findTestObject('Forgot Password/Forgot - button submit code'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Forgot Password/Forgot - button submit code'), 0)

Mobile.waitForElementPresent(findTestObject('Login/Login - button login'), GlobalVariable.g_timeout)

Mobile.closeApplication()

