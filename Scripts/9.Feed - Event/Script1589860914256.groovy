import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

not_run: Mobile.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

def scroll = false

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - text feeds'), GlobalVariable.g_timeout)

Mobile.verifyElementVisible(findTestObject('Feeds/Feeds - button PMM'), 0)

Mobile.verifyElementVisible(findTestObject('Feeds/Feeds - button scan QR'), 0)

def deviceHight = Mobile.getDeviceHeight()

def deviceWidth = Mobile.getDeviceWidth()

int startX = deviceWidth / 2

int endX = startX

int startY = deviceWidth

int endY = deviceWidth * 0.35

//swipe up : Mobile.swipe(startX, startY, endX, endY)
//swipe down : Mobile.swipe(startX, endY, endX, startY)
if (Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button card event'), 5, FailureHandling.OPTIONAL) == 
false) {
    Mobile.swipe(startX, startY, endX, endY)

    Mobile.swipe(startX, startY, endX, endY)

    scroll = true

    println(scroll)
}

if (Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button card event'), 5, FailureHandling.OPTIONAL) == 
true) {
    Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button card event'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Explore/Event/Event - button card event'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button back'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button share'), GlobalVariable.g_timeout)

    Mobile.verifyElementVisible(findTestObject('Detail Event/Event - text event'), 0)

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.comment('klik bookmark')

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Bookmark'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Detail Event/Event - button Bookmark'), 0)

    if (Mobile.waitForElementPresent(findTestObject('Detail Event/Bookmark - text popup add to calendar'), 3) == true) {
        Mobile.waitForElementPresent(findTestObject('Detail Event/Bookmark - button popup Later'), 0)

        Mobile.tap(findTestObject('Detail Event/Bookmark - button popup Later'), 0)
    }
    
    Mobile.comment('klik add to calendar')

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Calendar'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Detail Event/Event - button Calendar'), 0)

    if (Mobile.waitForElementPresent(findTestObject('Detail Event/Bookmark - text popup add to calendar'), 3) == true) {
        Mobile.waitForElementPresent(findTestObject('Detail Event/Bookmark - button popup Later'), 0)

        Mobile.tap(findTestObject('Detail Event/Bookmark - button popup Later'), 0)
    }
    
    Mobile.waitForElementPresent(findTestObject('Detail Event/Calendar - button Batal add'), 0)

    Mobile.tap(findTestObject('Detail Event/Calendar - button Batal add'), 0)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Calendar'), 0)

    Mobile.comment('unbookmark')

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Unbookmark'), 0)

    Mobile.tap(findTestObject('Detail Event/Event - button Unbookmark'), 0)

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    if (Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Certificate'), 3) == true) {
        Mobile.comment('buka certificate')

        Mobile.tap(findTestObject('Detail Event/Event - button Certificate'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - button back'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - download sertifikat manual'), GlobalVariable.g_timeout)

        Mobile.tap(findTestObject('CME/CME - download sertifikat manual'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - toast download berhasil manual'), GlobalVariable.g_timeout)

        Mobile.waitForElementPresent(findTestObject('CME/CME - button send email manual'), 0)

        Mobile.tap(findTestObject('CME/CME - button send email manual'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - toast sukses kirim email manual'), 5)

        Mobile.comment('back ke halaman detil event')

        Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CME/CME - button back'), 0)

        Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 0)
    }
    
    if (Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 3) == true) {
        Mobile.comment('buka materi')

        Mobile.tap(findTestObject('Detail Event/Event - button Materi'), 0)

        Mobile.waitForElementPresent(findTestObject('Detail Event/Materi - button back materi'), GlobalVariable.g_timeout)

        Mobile.waitForElementPresent(findTestObject('Detail Event/Materi - text Event Material'), 0)

        Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('Detail Event/Materi - button back materi'), 0)

        Mobile.comment('back ke halaman detil event')

        Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 0)
    }
    
    Mobile.comment('share')

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button share'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Detail Event/Event - button share'), 0)

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.pressBack()

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button back'), GlobalVariable.g_timeout)

    Mobile.comment('kembali ke list feed')

    Mobile.tap(findTestObject('Detail Event/Event - button back'), 0)

    if (scroll == true) {
        Mobile.swipe(startX, endY, endX, startY)

        Mobile.swipe(startX, endY, endX, startY)
    }
} else {
    println('Skip Karena Event tidak ditemukan di Feeds')
}

