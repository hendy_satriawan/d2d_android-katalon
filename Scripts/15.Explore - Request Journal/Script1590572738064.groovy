import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

def deviceHight = Mobile.getDeviceHeight()

def deviceWidth = Mobile.getDeviceWidth()

int startX = deviceWidth / 2

int endX = startX

int startY = deviceWidth

int endY = deviceWidth * 0.35

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - button menu explore'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Feeds/Feeds - button menu explore'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Explore - Menu Request'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Explore/Explore - Menu Request'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - text Request Journal'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - choose city'), 0)

Mobile.tap(findTestObject('Explore/Request Journal/Request - choose city'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/City - text kabupaten aceh barat'), 0)

Mobile.tap(findTestObject('Explore/Request Journal/City - text kabupaten aceh barat'), 0)

if (Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - input title journal'), 5, FailureHandling.OPTIONAL) == 
false) {
    Mobile.swipe(startX, startY, endX, endY)

    Mobile.swipe(startX, startY, endX, endY)
}

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - input title journal'), 0)

Mobile.setText(findTestObject('Explore/Request Journal/Request - input title journal'), v_judul_request_journal, 0)

if (Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - button Submit'), 5, FailureHandling.OPTIONAL) == 
false) {
    Mobile.swipe(startX, startY, endX, endY)

    Mobile.swipe(startX, startY, endX, endY)
}

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - button Submit'), 0)

Mobile.tap(findTestObject('Explore/Request Journal/Request - button Submit'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - text request journal berhasil'), 0)

