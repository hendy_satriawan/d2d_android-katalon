import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - button menu CME'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Feeds/Feeds - button menu CME'), 0)

if (Mobile.waitForElementPresent(findTestObject('CME/CME - button add SKP courses'), 5, FailureHandling.OPTIONAL) == true) {
    Mobile.waitForElementPresent(findTestObject('CME/CME - button History CME'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('CME/CME - text CME'), GlobalVariable.g_timeout)

    Mobile.comment('isi kuis')

    Mobile.waitForElementPresent(findTestObject('CME/CME - button card quiz'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('CME/CME - button card quiz'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - button back'), 0)

    if (Mobile.waitForElementPresent(findTestObject('CME/CME - button start quiz'), 5, FailureHandling.OPTIONAL) == true) {
        Mobile.waitForElementPresent(findTestObject('CME/CME - button start quiz'), 0)

        Mobile.comment('isi kuis')

        Mobile.tap(findTestObject('CME/CME - button start quiz'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - button back'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - text Questions'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - button radio 0'), 0)

        Mobile.tap(findTestObject('CME/CME - button radio 0'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - button Submit Quiz'), 0)

        Mobile.tap(findTestObject('CME/CME - button Submit Quiz'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - text popup you may try again'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - button popup failed OK'), 0)

        Mobile.tap(findTestObject('CME/CME - button popup failed OK'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - text SKP this year'), 0)
    } else {
        Mobile.waitForElementPresent(findTestObject('CME/CME - button back'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - download sertifikat manual'), GlobalVariable.g_timeout)

        Mobile.tap(findTestObject('CME/CME - download sertifikat manual'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - toast download berhasil manual'), GlobalVariable.g_timeout)

        Mobile.waitForElementPresent(findTestObject('CME/CME - button send email manual'), 0)

        Mobile.tap(findTestObject('CME/CME - button send email manual'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - toast sukses kirim email manual'), 5)

        Mobile.comment('back ke halaman cme')

        Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CME/CME - button back'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - text SKP this year'), 0)
    }
    
    Mobile.comment('add skp manual')

    Mobile.waitForElementPresent(findTestObject('CME/CME - button add SKP courses'), 0)

    Mobile.tap(findTestObject('CME/CME - button add SKP courses'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - text title CME Course'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - button back'), 0)

    Mobile.comment('input data course manual')

    Mobile.waitForElementPresent(findTestObject('CME/CME - input judul cme manual'), 0)

    Mobile.setText(findTestObject('CME/CME - input judul cme manual'), v_judul_skp, 0)

    Mobile.setText(findTestObject('CME/CME - input source cme manual'), v_source, 0)

    Mobile.setText(findTestObject('CME/CME - input skp cme manual'), v_poin, 0)

    Mobile.comment('input tanggal')

    Mobile.tap(findTestObject('CME/CME - input date cme manual'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/popup date - text tahun sekarang'), 0)

    Mobile.tap(findTestObject('CME/popup date - button ok'), 0)

    Mobile.comment('upload gambar')

    Mobile.tap(findTestObject('CME/CME - text Upload Certificate manual'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/popup upload - button pilih camera'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('CME/popup upload - button pilih camera'), 0)

    Mobile.waitForElementPresent(findTestObject('Extra/camera - tombol shutter'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Extra/camera - tombol shutter'), 0)

    Mobile.waitForElementPresent(findTestObject('Extra/camera - button ok'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Extra/camera - button ok'), 0)

    Mobile.tap(findTestObject('CME/CME - button upload cme manual'), 0)

    Mobile.comment('sukses upload sertifikat manual')

    Mobile.waitForElementPresent(findTestObject('CME/CME - button back'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - download sertifikat manual'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('CME/CME - download sertifikat manual'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - toast download berhasil manual'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('CME/CME - button send email manual'), 0)

    Mobile.tap(findTestObject('CME/CME - button send email manual'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - toast sukses kirim email manual'), 5)

    Mobile.comment('back ke halaman cme')

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CME/CME - button back'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - text SKP this year'), 0)

    Mobile.comment('history cme')

    Mobile.waitForElementPresent(findTestObject('CME/CME - button History CME'), 0)

    Mobile.tap(findTestObject('CME/CME - button History CME'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - text title history'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - tab D2D SKP'), 0)

    Mobile.tap(findTestObject('CME/CME - tab D2D SKP'), 0)

    Mobile.tap(findTestObject('CME/CME - tab Manual SKP'), 0)

    Mobile.tap(findTestObject('CME/CME - button back'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - text SKP this year'), 0)
} else {
    println('User belum input NPA IDI')
}

