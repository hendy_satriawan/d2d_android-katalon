<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>15</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>90aaf5a5-50ce-4eb8-b585-1e1d2dfeb47c</testSuiteGuid>
   <testCaseLink>
      <guid>40825542-958b-4e06-ad09-fac4010c8a4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/1.Register Complete</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9dab4e6e-71f1-43da-89ca-60646839a91f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5c60fdf5-7277-4615-af0e-323bcbfb482a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Login Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>952dec24-db7c-4907-981a-721a6ace35e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3.Login Facebook</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d305f96e-0a1a-4d84-93ce-527bfedf5b27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/4.Login Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>357b52b5-7e33-45cd-9cbe-19f4e6e7881b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5.Forgot Password</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cfa47e11-603f-45e6-8b59-80e422e99fab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>eb26f79c-a65b-47cb-9086-55eb4181dde1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/6.Feed - Journal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0630e02-6c2c-4c87-856e-80dcdc74bea6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.Feed - Guideline</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37e9e71f-4ced-4101-942f-c38f738b5cb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/8.Feed - Video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01cba85b-7060-4e88-8a0c-c9b21b1d823a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/9.Feed - Event</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53bbc650-9db6-412a-a780-b924563cc497</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10.Feed - Scan QR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11d8bee9-39ce-49dd-8f04-7a27f4a62f08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11.Feed - PMM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75acbfa6-594d-4950-b187-d5a15d05df87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12.CME</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7702aacc-78c5-4479-b8c1-f94e7268ef20</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ed746076-19cd-4288-ac5a-35a419ce1787</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>65c95812-f0ef-4c5f-8cd9-2c73955eb36b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>70722b94-bb75-460e-9984-92c529cf2753</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/13.Explore - Event</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a4c3579-4863-4b19-88e1-3757537d3b09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14.Explore - Learning</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>531130f6-d6d7-412e-89c1-c7ff493c42c5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f4d4bac5-0512-43a7-82ed-ddbe33f36ce0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7f5ba1da-9836-4073-9b46-c872dee9e33b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/15.Explore - Request Journal</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6f1a7e94-e1ef-4722-a189-93a32cc5c2c2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2de9f9dd-b8ea-4893-acd2-dfe46f2061be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/16.Explore - Obat A to Z</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1b43d208-c452-40cb-9d1a-8cc312240af5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a8a18944-5364-4155-a55d-b546c5aa7865</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/17.Webinar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92a7eebc-8e6a-41e1-9291-16a78281e2a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/18. Profile</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>38a8c52b-662d-4d6f-9fa9-2f725f85737d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
